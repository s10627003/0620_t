package sample;

import javafx.beans.property.SimpleStringProperty;

public class GetFile {
    private final SimpleStringProperty fileName;
    private final SimpleStringProperty fileExt;
    private final SimpleStringProperty fileSize;
    private final SimpleStringProperty fileDate;

    GetFile(String fileN, String fileE, String fileS, String fileD){
        this.fileName = new SimpleStringProperty(fileN);
        this.fileExt = new SimpleStringProperty(fileE);
        this.fileSize = new SimpleStringProperty(fileS);
        this.fileDate = new SimpleStringProperty(fileD);

    }
    public void setFileName(String fileName) {
        this.fileName.set(fileName);
    }

    public void setFileExt(String fileExt) {
        this.fileExt.set(fileExt);
    }

    public void setFileSize(String fileSize) {
        this.fileSize.set(fileSize);
    }

    public void setFileDate(String fileDate) {
        this.fileDate.set(fileDate);
    }

    public String getFileName() {
        return fileName.get();
    }

    public SimpleStringProperty fileNameProperty() {
        return fileName;
    }

    public String getFileExt() {
        return fileExt.get();
    }

    public SimpleStringProperty fileExtProperty() {
        return fileExt;
    }

    public String getFileSize() {
        return fileSize.get();
    }

    public SimpleStringProperty fileSizeProperty() {
        return fileSize;
    }

    public String getFileDate() {
        return fileDate.get();
    }

    public SimpleStringProperty fileDateProperty() {
        return fileDate;
    }
}
