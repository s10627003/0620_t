package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.stage.Stage;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ComboBox device;
    public TableView fileTbl;

    ObservableList data = FXCollections.observableArrayList();


    public void sel_dev(ActionEvent actionEvent) throws IOException {
    }
    public void on_file(MouseEvent mouseEvent) {
    }

    public void test_on(ActionEvent actionEvent) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        File[] roots = File.listRoots();
        FileSystemView fSysView = FileSystemView.getFileSystemView();
        ArrayList arrayList=new ArrayList();
        for (int i = 0; i < roots.length; i++)
            arrayList.add(roots[i]);
        device.setItems(FXCollections.observableArrayList(arrayList));

        StringBuffer fileList = new StringBuffer();
        fileTbl.setEditable(true);
        try{
            java.io.File folder = new java.io.File(roots[0].toURI());
            String[] list = folder.list();
            String[] listE = folder.list();
            String[] listS = folder.list();
            String[] listD = folder.list();
            for(int i = 0; i < list.length; i++){
                System.out.println(list[i]);
                data.add(new GetFile(list[i],"","",""));
            }
            //fileTbl.getColumns().addAll("name","url","other");

            TableColumn firstNameCol = new TableColumn("File Name");
            firstNameCol.setMinWidth(100);
            firstNameCol.setCellValueFactory(new PropertyValueFactory("fileName"));
            firstNameCol.setCellFactory(TextFieldTableCell.forTableColumn());

            fileTbl.setItems(data);
            fileTbl.getColumns().addAll(firstNameCol);
            /*
            TableColumn fileExt = new TableColumn("File Ext");
            firstNameCol.setMinWidth(100);
            firstNameCol.setCellValueFactory(new PropertyValueFactory("fileExt"));
            firstNameCol.setCellFactory(TextFieldTableCell.forTableColumn());

            TableColumn fileSize = new TableColumn("File Size");
            firstNameCol.setMinWidth(100);
            firstNameCol.setCellValueFactory(new PropertyValueFactory("fileSize"));
            firstNameCol.setCellFactory(TextFieldTableCell.forTableColumn());

            TableColumn fileDate = new TableColumn("File Date");
            firstNameCol.setMinWidth(100);
            firstNameCol.setCellValueFactory(new PropertyValueFactory("fileDate"));
            firstNameCol.setCellFactory(TextFieldTableCell.forTableColumn());

            */





        }catch(Exception e){
            System.out.println("'"+roots[0].toURI()+"'此資料夾不存在");
        }
    }
}
